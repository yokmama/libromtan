package jp.co.kayo.android.offeringtodo.bean;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OfferingListBean implements JsonParsable {
    private ArrayList<OfferingBean> offerings;

    public static OfferingListBean parse(String raw) throws JSONException {
        OfferingListBean offeringListBean = new OfferingListBean();
        ArrayList<OfferingBean> list = new ArrayList<OfferingBean>();

        JSONObject root = new JSONObject(raw);
        JSONArray jsonArray = root.getJSONArray("offerings");
        for (int i = 0, iL = jsonArray.length(); i < iL; i++) {
            JSONObject ofrJson = jsonArray.getJSONObject(i);

            OfferingBean bean = new OfferingBean();
            bean.setName(ofrJson.getString("name"));
            bean.setPower(ofrJson.getString("power"));
            JSONArray starArray = ofrJson.getJSONArray("star");
            bean.setStar(new String[] {
                    starArray.getString(0),
                    starArray.getString(1),
                    starArray.getString(2),
                    starArray.getString(3),
            });
            bean.setEffectTime(ofrJson.getString("effect_time"));
            //bean.setHowToGet(ofrJson.getString("how_to_get"));
            bean.setEffect(ofrJson.getString("effect"));

            list.add(bean);
        }
        
        offeringListBean.setOfferings(list);

        return offeringListBean;
    }

    public ArrayList<OfferingBean> getOfferings() {
        return offerings;
    }

    public void setOfferings(ArrayList<OfferingBean> offerings) {
        this.offerings = offerings;
    }

}
