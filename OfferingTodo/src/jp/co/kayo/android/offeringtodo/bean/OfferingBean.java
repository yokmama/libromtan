
package jp.co.kayo.android.offeringtodo.bean;


/**
 * 供物データ
 * 
 * @author esperia09
 */
public class OfferingBean implements JsonParsable {

    private int id;
    private String name;
    private String power;
    private String[] star;
    private String effectTime;
    private String howToGet;
    private String effect;
    /** @deprecated */
    private String img;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String[] getStar() {
        return star;
    }

    public void setStar(String[] star) {
        this.star = star;
    }

    public String getEffectTime() {
        return effectTime;
    }

    public void setEffectTime(String effectTime) {
        this.effectTime = effectTime;
    }

    public String getHowToGet() {
        return howToGet;
    }

    public void setHowToGet(String howToGet) {
        this.howToGet = howToGet;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }
}
