package jp.co.kayo.android.offeringtodo.bean;

import org.json.JSONArray;
import org.json.JSONException;

public class CategoriesBean implements JsonParsable {

    private String[] categories;

    public static CategoriesBean parse(String raw) throws JSONException {
        CategoriesBean bean = new CategoriesBean();
        
        JSONArray jsonArray = new JSONArray(raw);
        String[] kinds = new String[jsonArray.length()];
        for (int i=0, iL = jsonArray.length(); i<iL; i++) {
            kinds[i] = jsonArray.getString(i);
        }
        bean.setCategories(kinds);
        
        return bean;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] kinds) {
        this.categories = kinds;
    }

}
