package jp.co.kayo.android.offeringtodo.db;

public interface TableColumns {

    public static interface BaseColumns {
        public static final String _ID = "_id";
    }

    public static interface LastUpdate extends BaseColumns {
        public static final String TBNAME = "t_lastupdate";
        public static final String DATE = "f_date";
    }

    public static interface Category extends BaseColumns {
        public static final String TBNAME = "t_category";
        public static final String NAME = "f_name";
        public static final String DESCRIPTION = "f_description";
    }

    public static interface Attribute extends BaseColumns {
        public static final String TBNAME = "t_attribute";
        public static final String NAME = "f_name";
        public static final String DESCRIPTION = "f_description";
    }

    public static interface Effect extends BaseColumns {
        public static final String TBNAME = "t_effect";
        public static final String NAME = "f_name";
        public static final String DESCRIPTION = "f_description";
    }

    public static interface Sacrifice extends BaseColumns {
        public static final String TBNAME = "t_sacrifice";
        public static final String NAME = "f_name";
        public static final String CATEGORY_ID = "f_category_id";
        public static final String ATTRIBUTE_ID = "f_attribute_id";
        public static final String EFFECT_ID = "f_effect_id";
        public static final String POWER = "f_power";
        public static final String NUMBER = "f_number";
        public static final String EFFECT_TIME = "f_effect_time";
        public static final String IMAGE = "f_image";
    }

    public static interface SacrificeDescription extends BaseColumns {
        public static final String TBNAME = "t_sacrifice_description";
        public static final String NAME = "f_name";
        public static final String SACRIFICE_ID = "f_sacrifice_id";
        public static final String DESCRIPTION = "f_description";
    }

    public static interface Index extends BaseColumns {
        public static final String TBNAME = "t_index";
        public static final String NAME = "f_name";
        public static final String PARENT_ID = "f_parent_id";
        public static final String DESCRIPTION = "f_description";
    }

    public static interface Quest extends BaseColumns {
        public static final String TBNAME = "t_quest";
        public static final String NAME = "f_name";
        public static final String INDEX_ID = "f_index_id";
        public static final String DESCRIPTION = "f_description";
    }

    public static interface QuestSacrifice extends BaseColumns {
        public static final String TBNAME = "t_quest_sacrifice";
        public static final String QUEST_ID = "f_quest_id";
        public static final String SACRIFICE_ID = "f_sacrifice_id";
    }

    public static interface Enemy extends BaseColumns {
        public static final int TYPE_HUMANOID = 0;
        public static final int TYPE_LOWLEVEL = 1;
        public static final int TYPE_HOSTILITY = 2;

        public static final String TBNAME = "t_enemy";
        public static final String NAME = "f_name";
        public static final String TYPE = "f_type";
        public static final String DESCRIPTION = "f_description";
    }

    public static interface QuestEnemy extends BaseColumns {
        public static final String TBNAME = "t_quest_enemy";
        public static final String QUEST_ID = "f_quest_id";
        public static final String ENEMY_ID = "f_enemy_id";
    }

    public static interface EnemyAttribute extends BaseColumns {
        public static final String TBNAME = "t_enemy_attribute";
        public static final String ENEMY_ID = "f_enemy_id";
        public static final String ATTRIBUTE_ID = "f_attribute_id";
    }

    public static interface EnemyWeakAttribute extends BaseColumns {
        public static final String TBNAME = "t_enemy_weak_attribute";
        public static final String ENEMY_ID = "f_enemy_id";
        public static final String ATTRIBUTE_ID = "f_attribute_id";
    }

}
