
package jp.co.kayo.android.offeringtodo;

import java.util.ArrayList;

import jp.co.kayo.android.offeringtodo.bean.OfferingBean;
import jp.co.kayo.android.offeringtodo.db.SoulSacrificeContentProvider;
import jp.co.kayo.android.offeringtodo.db.TableColumns;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class OfferingsListFragment extends ListFragment implements
        LoaderCallbacks<ArrayList<OfferingBean>> {

    protected ArrayList<OfferingBean> mOfferings;
    private OfferingListAdapter mAdapter;

    public static OfferingsListFragment newInstance() {
        OfferingsListFragment f = new OfferingsListFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_offering, container, false);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getListView().setFastScrollEnabled(true);

        getLoaderManager().initLoader(R.layout.activity_offering,
                null, this);
    }

    /**
     * 進捗ダイアログ
     */
    public static class ProgressDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            ProgressDialog dlg = new ProgressDialog(getActivity());
            dlg.setMessage(this.getString(R.string.message_getting_offerings));
            return dlg;
        }
    }

    @Override
    public Loader<ArrayList<OfferingBean>> onCreateLoader(int arg0, Bundle arg1) {
        OfferingListLoader offeringListLoader = new OfferingListLoader(getActivity());
        offeringListLoader.forceLoad();
        return offeringListLoader;
    }

    protected void setList() {
        OfferingListAdapter adapter = new OfferingListAdapter(
                getActivity().getApplicationContext(), mOfferings);
        setListAdapter(adapter);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<OfferingBean>> loader, ArrayList<OfferingBean> data) {
        mAdapter = new OfferingListAdapter(getActivity(), data);
        getListView().setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<OfferingBean>> loader) {
        mAdapter = new OfferingListAdapter(getActivity(),
                new ArrayList<OfferingBean>());
        getListView().setAdapter(mAdapter);

    }

    private static class OfferingListLoader extends AsyncTaskLoader<ArrayList<OfferingBean>>
            implements TableColumns {

        public OfferingListLoader(Context context) {
            super(context);
        }

        @Override
        public ArrayList<OfferingBean> loadInBackground() {
            ArrayList<OfferingBean> result = new ArrayList<OfferingBean>();

            Cursor c = getContext().getContentResolver().query(
                    SoulSacrificeContentProvider.SACRIFICE_CONTENT_URI,
                    new String[] {
                            Sacrifice._ID, Sacrifice.NAME
                    }, null, null, Sacrifice._ID + " DESC");
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        int id = (int) c.getLong(0);
                        String name = c.getString(1);

                        OfferingBean offeringBean = new OfferingBean();
                        offeringBean.setId(id);
                        offeringBean.setName(name);

                        result.add(offeringBean);
                    } while (c.moveToNext());
                }
                c.close();
            }
            return result;
        }
    }
}
