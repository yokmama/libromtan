
package jp.co.kayo.android.offeringtodo.net;

import java.io.IOException;

import jp.co.kayo.android.offeringtodo.bean.OfferingListBean;
import jp.co.kayo.android.offeringtodo.utils.Consts;
import jp.co.kayo.android.offeringtodo.utils.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import android.os.AsyncTask;

/**
 * 
 * @author esperia09
 * @deprecated 
 */
public class OfferingsAsyncTask extends AsyncTask<String, Void, OfferingListBean> {
    private OfferingsAsyncTaskListener mListener;
    private Throwable mException;

    private String getAbsoluteUrl(String urlPath) {
        return Consts.BASE_URL + urlPath;
    }

    @Override
    protected OfferingListBean doInBackground(String... params) {
        String absoluteUrl = getAbsoluteUrl("ss/offerings");

        HttpGet request = new HttpGet(absoluteUrl);

        // HttpClientインタフェースではなくて、実クラスのDefaultHttpClientを使う。
        // 実クラスでないとCookieが使えないなど不都合が多い。
        DefaultHttpClient httpClient = new DefaultHttpClient();

        try {
            String result = httpClient.execute(request, new ResponseHandler<String>() {
                @Override
                public String handleResponse(HttpResponse response)
                        throws ClientProtocolException, IOException {

                    // response.getStatusLine().getStatusCode()でレスポンスコードを判定する。
                    // 正常に通信できた場合、HttpStatus.SC_OK（HTTP 200）となる。
                    switch (response.getStatusLine().getStatusCode()) {
                        case HttpStatus.SC_OK:
                            // レスポンスデータを文字列として取得する。
                            // byte[]として読み出したいときはEntityUtils.toByteArray()を使う。
                            return EntityUtils.toString(response.getEntity(), "UTF-8");

                        case HttpStatus.SC_NOT_FOUND:
                            throw new RuntimeException("データないよ！"); // FIXME

                        default:
                            throw new RuntimeException("なんか通信エラーでた"); // FIXME
                    }

                }
            });

            // logcatにレスポンスを表示
            Logger.d(result);

            try {
                OfferingListBean parse = OfferingListBean.parse(result);
                return parse;
            } catch (JSONException e) {
                mException = e;
                Logger.e("JSONのパースに失敗", e);
            }
        } catch (RuntimeException e) {
            mException = e;
        } catch (ClientProtocolException e) {
            mException = e;
        } catch (IOException e) {
            mException = e;
        } finally {
            // ここではfinallyでshutdown()しているが、HttpClientを使い回す場合は、
            // 適切なところで行うこと。当然だがshutdown()したインスタンスは通信できなくなる。
            httpClient.getConnectionManager().shutdown();
        }
        return null;
    }

    public void setListener(OfferingsAsyncTaskListener l) {
        mListener = l;
    }

    @Override
    protected void onPostExecute(OfferingListBean result) {
        super.onPostExecute(result);

        if (result != null) {
            if (mListener != null) {
                mListener.onSuccess(result);
            }
        } else {
            if (mListener != null) {
                mListener.onError(mException);
            }
        }
    }

    public interface OfferingsAsyncTaskListener {
        public void onSuccess(OfferingListBean offerings);

        public void onError(Throwable e);
    }

}
