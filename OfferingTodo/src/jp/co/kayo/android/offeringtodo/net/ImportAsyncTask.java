
package jp.co.kayo.android.offeringtodo.net;

import java.io.IOException;

import jp.co.kayo.android.offeringtodo.bean.JsonParsable;
import jp.co.kayo.android.offeringtodo.bean.CategoriesBean;
import jp.co.kayo.android.offeringtodo.bean.OfferingListBean;
import jp.co.kayo.android.offeringtodo.utils.Consts;
import jp.co.kayo.android.offeringtodo.utils.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import android.os.AsyncTask;

/**
 * データをインポートします。
 * @author esperia09
 */
public class ImportAsyncTask extends AsyncTask<String, JsonParsable, Void> {
    public static final String URL_OFFERINGS = "ss/offerings";
    public static final String URL_KINDS = "ss/kinds";
    
    private ImportAsyncTaskListener mListener;
    private Throwable mException;

    private String getAbsoluteUrl(String urlPath) {
        return Consts.BASE_URL + urlPath;
    }

    @Override
    protected Void doInBackground(String... params) {
        DefaultHttpClient httpClient = new DefaultHttpClient();

        try {
            // 供物リスト取得
            {
                HttpGet request = new HttpGet(getAbsoluteUrl(URL_OFFERINGS));
                String result = receive(httpClient, request);
                OfferingListBean parse = OfferingListBean.parse(result);
                publishProgress(parse);
            }

            // 供物の種類取得
            {
                HttpGet request = new HttpGet(getAbsoluteUrl(URL_KINDS));
                String result = receive(httpClient, request);
                CategoriesBean parse = CategoriesBean.parse(result);
                publishProgress(parse);
            }
        } catch (RuntimeException e) {
            mException = e;
        } catch (JSONException e) {
            mException = e;
        } catch (ClientProtocolException e) {
            mException = e;
        } catch (IOException e) {
            mException = e;
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return null;
    }
    
    private String receive(HttpClient httpClient, HttpRequestBase request) throws ClientProtocolException, IOException {
        String result = httpClient.execute(request, new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {

                // response.getStatusLine().getStatusCode()でレスポンスコードを判定する。
                // 正常に通信できた場合、HttpStatus.SC_OK（HTTP 200）となる。
                switch (response.getStatusLine().getStatusCode()) {
                    case HttpStatus.SC_OK:
                        // レスポンスデータを文字列として取得する。
                        // byte[]として読み出したいときはEntityUtils.toByteArray()を使う。
                        return EntityUtils.toString(response.getEntity(), "UTF-8");

                    case HttpStatus.SC_NOT_FOUND:
                        throw new RuntimeException("データないよ！"); // FIXME

                    default:
                        throw new RuntimeException("なんか通信エラーでた"); // FIXME
                }

            }
        });

        // logcatにレスポンスを表示
        Logger.d(result);
        return result;
    }
    
    public void setListener(ImportAsyncTaskListener l) {
        mListener = l;
    }
    
    @Override
    protected void onProgressUpdate(JsonParsable... values) {
        JsonParsable jsonParsable = values[0];
        if (mListener != null) {
            mListener.onReceived(jsonParsable);
        }
        
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        if (mException == null) {
            if (mListener != null) {
                mListener.onSuccess();
            }
        } else {
            if (mListener != null) {
                mListener.onError(mException);
            }
        }
    }

    public interface ImportAsyncTaskListener {
        /**
         * データが一つ取得できた場合
         */
        public void onReceived(JsonParsable parsedBeans);

        /**
         * インポートが∀終了した場合
         */
        public void onSuccess();
        
        /**
         * 途中でエラーが発生した場合
         * @param e
         */
        public void onError(Throwable e);
    }

    public static class ImportBean {
        public int url;
        public String json;
    }
}
