
package jp.co.kayo.android.offeringtodo;

import java.util.ArrayList;
import java.util.Iterator;

import jp.co.kayo.android.offeringtodo.bean.JsonParsable;
import jp.co.kayo.android.offeringtodo.bean.CategoriesBean;
import jp.co.kayo.android.offeringtodo.bean.OfferingBean;
import jp.co.kayo.android.offeringtodo.bean.OfferingListBean;
import jp.co.kayo.android.offeringtodo.db.SoulSacrificeContentProvider;
import jp.co.kayo.android.offeringtodo.db.TableColumns;
import jp.co.kayo.android.offeringtodo.net.ImportAsyncTask;
import jp.co.kayo.android.offeringtodo.net.ImportAsyncTask.ImportAsyncTaskListener;
import jp.co.kayo.android.offeringtodo.utils.Logger;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

public class ImportDialogFragment extends DialogFragment {

    public static ImportDialogFragment newInstance() {
        ImportDialogFragment f = new ImportDialogFragment();

        return f;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dlg = new ProgressDialog(getActivity());
        dlg.setMessage(this.getString(R.string.message_getting_offerings));
        return dlg;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        requestOfferingsData();
    }

    void requestOfferingsData() {
        // setEmptyText(context.getString(R.string.message_getting_offerings));
        ImportAsyncTask task = new ImportAsyncTask();
        task.setListener(new ImportAsyncTaskListener() {

            private void onFinish() {
                dismiss();
            }

            @Override
            public void onReceived(JsonParsable parsedBeans) {
                ProgressDialog dialog = (ProgressDialog) getDialog();
                Context context = dialog.getContext();
                if (parsedBeans instanceof OfferingListBean) {
                    OfferingListBean bean = (OfferingListBean) parsedBeans;
                    // TODO: DBへ
                    ArrayList<OfferingBean> offerings = bean.getOfferings();
                    
                    ContentResolver contentResolver = context.getContentResolver();
                    for (Iterator<OfferingBean> ite = offerings.iterator(); ite.hasNext();) {
                        OfferingBean offeringBean = ite.next();
                        
                        ContentValues values = new ContentValues();
                        values.put(TableColumns.Sacrifice.NAME, offeringBean.getName());
                        contentResolver.insert(SoulSacrificeContentProvider.SACRIFICE_CONTENT_URI, values);
                    }
                    
                    dialog.setMessage(context.getString(R.string.message_getting_kinds));
                } else if (parsedBeans instanceof CategoriesBean) {
                    CategoriesBean bean = (CategoriesBean) parsedBeans;
                    // TODO: DBへ

                    ContentResolver contentResolver = context.getContentResolver();
                    String[] kinds = bean.getCategories();
                    for (String kind : kinds) {
                        ContentValues values = new ContentValues();
                        values.put(TableColumns.Category.NAME, kind);
                        contentResolver.insert(SoulSacrificeContentProvider.CATEGORY_CONTENT_URI, values);
                    }
                }
                else {
                    throw new RuntimeException("Unexpected bean");
                }
            }

            @Override
            public void onSuccess() {
                onFinish();
            }

            @Override
            public void onError(Throwable e) {
                // TODO
                Logger.e("通信失敗", e);
                onFinish();
                Toast.makeText(getActivity(), "通信失敗", Toast.LENGTH_SHORT).show();
            }
        });
        task.execute();
    }

}
