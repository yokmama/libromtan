<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Sacrifice $Sacrifice
 */
class Category extends AppModel {

    public $messages;

    public function init() {
        $this->messages = array();

        $ssConsts = Configure::read('soulsacrifice');
        foreach ($ssConsts['offerings'] as $key => $value) {
            $this->create();
            $this->set(array(
                'name' => $value
            ));
            if (!$this->save()) {
                $this->messages[] = 'カテゴリの保存に失敗しました。';
                $this->log('validationErrors', LOG_DEBUG);
                $this->log($this->validationErrors, LOG_DEBUG);
                return false;
            }
        }
        return true;
    }

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notempty' => array(
				//'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Sacrifice' => array(
			'className' => 'Sacrifice',
			'foreignKey' => 'category_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
