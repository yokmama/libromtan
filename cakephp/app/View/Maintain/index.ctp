<?php

?><h2>メンテナンス</h2>
<h3>初期化</h3>
<div>
    <p>全てのデータを削除し、初期化します。</p>
</div>
<?php echo $this->Form->create(null, array(
    'type' => 'post',
    'url' => array(
        'controller' => 'maintain',
        'action' => 'init'
    )
)); ?><?php echo $this->Form->end('リセット'); ?>