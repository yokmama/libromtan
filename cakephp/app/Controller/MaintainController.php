<?php
App::uses('AppController', 'Controller');
/**
 * Maintain Controller
 *
 * @property Maintain $Maintain
 */
class MaintainController extends AppController
{
    var $uses = array('Category');

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
    }

    public function init()
    {
        if (!$this->request->is('post')) {
            $this->redirect('index');
            return;
        }
        $this->view = 'console';

        $messages = array();
        $messages[] = '開始します。';

        $this->Category->query('TRUNCATE categories');

        $messages[] = 'Categoryの初期化';
        if (!$this->Category->init()) {
            $messages[] = '初期化失敗';
            array_merge($messages, $this->Category->messages);
        }

        $messages[] = '終了しました。';
        $this->set('messages', $messages);
    }

}