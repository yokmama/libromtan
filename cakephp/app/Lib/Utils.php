<?php

/**
 * App::uses('Utils', 'Lib');
 * User: esperia09
 * Date: 2013/04/08
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */
class Utils
{

    function __construct() {
    }

    public static function trimSpaces($s) {
        $s = trim($s);
        $s = str_replace('&nbsp;', '', $s);
        return $s;
    }
}