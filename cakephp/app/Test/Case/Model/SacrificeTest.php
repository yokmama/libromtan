<?php
App::uses('Sacrifice', 'Model');

/**
 * Sacrifice Test Case
 *
 */
class SacrificeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sacrifice',
		'app.category',
		'app.attribute',
		'app.effect',
		'app.sacrifice_description'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sacrifice = ClassRegistry::init('Sacrifice');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sacrifice);

		parent::tearDown();
	}

}
