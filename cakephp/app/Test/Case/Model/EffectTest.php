<?php
App::uses('Effect', 'Model');

/**
 * Effect Test Case
 *
 */
class EffectTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.effect',
		'app.sacrifice',
		'app.category',
		'app.attribute',
		'app.sacrifice_description'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Effect = ClassRegistry::init('Effect');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Effect);

		parent::tearDown();
	}

}
