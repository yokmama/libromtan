<?php
App::uses('Lastupdate', 'Model');

/**
 * Lastupdate Test Case
 *
 */
class LastupdateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.lastupdate'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Lastupdate = ClassRegistry::init('Lastupdate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Lastupdate);

		parent::tearDown();
	}

}
