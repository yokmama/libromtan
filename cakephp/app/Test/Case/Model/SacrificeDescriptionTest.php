<?php
App::uses('SacrificeDescription', 'Model');

/**
 * SacrificeDescription Test Case
 *
 */
class SacrificeDescriptionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sacrifice_description',
		'app.sacrifice',
		'app.category',
		'app.attribute',
		'app.effect'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SacrificeDescription = ClassRegistry::init('SacrificeDescription');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SacrificeDescription);

		parent::tearDown();
	}

}
