<?php
App::uses('MaintainController', 'Controller');

/**
 * MaintainController Test Case
 *
 */
class MaintainControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.maintain'
	);

}
