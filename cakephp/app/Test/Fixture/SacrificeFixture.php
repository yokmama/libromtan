<?php
/**
 * SacrificeFixture
 *
 */
class SacrificeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'category_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'attribute_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'effect_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'power' => array('type' => 'integer', 'null' => false, 'default' => null),
		'number' => array('type' => 'integer', 'null' => false, 'default' => null),
		'effect_time' => array('type' => 'integer', 'null' => false, 'default' => null),
		'image' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'category_id' => 1,
			'attribute_id' => 1,
			'effect_id' => 1,
			'power' => 1,
			'number' => 1,
			'effect_time' => 1,
			'image' => 'Lorem ipsum dolor sit amet',
			'modified' => '2013-04-14 23:22:51',
			'created' => '2013-04-14 23:22:51'
		),
	);

}
