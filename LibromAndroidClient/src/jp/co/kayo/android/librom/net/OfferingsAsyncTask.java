
package jp.co.kayo.android.librom.net;

import java.io.IOException;
import java.util.ArrayList;

import jp.co.kayo.android.librom.bean.OfferingBean;
import jp.co.kayo.android.librom.utils.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import android.os.AsyncTask;

public class OfferingsAsyncTask extends AsyncTask<String, Void, ArrayList<OfferingBean>> {
    private static final String BASE_URL = "http://libromtan.herokuapp.com/";
    private OfferingsAsyncTaskListener mListener;
    private Throwable mException;

    private String getAbsoluteUrl(String urlPath) {
        return BASE_URL + urlPath;
    }

    @Override
    protected ArrayList<OfferingBean> doInBackground(String... params) {
        String absoluteUrl = getAbsoluteUrl("ss/offerings");

        HttpGet request = new HttpGet(absoluteUrl);

        // HttpClientインタフェースではなくて、実クラスのDefaultHttpClientを使う。
        // 実クラスでないとCookieが使えないなど不都合が多い。
        DefaultHttpClient httpClient = new DefaultHttpClient();

        try {
            String result = httpClient.execute(request, new ResponseHandler<String>() {
                @Override
                public String handleResponse(HttpResponse response)
                        throws ClientProtocolException, IOException {

                    // response.getStatusLine().getStatusCode()でレスポンスコードを判定する。
                    // 正常に通信できた場合、HttpStatus.SC_OK（HTTP 200）となる。
                    switch (response.getStatusLine().getStatusCode()) {
                        case HttpStatus.SC_OK:
                            // レスポンスデータを文字列として取得する。
                            // byte[]として読み出したいときはEntityUtils.toByteArray()を使う。
                            return EntityUtils.toString(response.getEntity(), "UTF-8");

                        case HttpStatus.SC_NOT_FOUND:
                            throw new RuntimeException("データないよ！"); // FIXME

                        default:
                            throw new RuntimeException("なんか通信エラーでた"); // FIXME
                    }

                }
            });

            // logcatにレスポンスを表示
            Logger.d(result);
            
            ArrayList<OfferingBean> parse;
            try {
                parse = OfferingBean.parse(result);
                return parse;
            } catch (JSONException e) {
                mException = e;
                Logger.e("JSONのパースに失敗", e);
            }
        } catch (ClientProtocolException e) {
            mException = e;
        } catch (IOException e) {
            mException = e;
        } finally {
            // ここではfinallyでshutdown()しているが、HttpClientを使い回す場合は、
            // 適切なところで行うこと。当然だがshutdown()したインスタンスは通信できなくなる。
            httpClient.getConnectionManager().shutdown();
        }
        return null;
    }
    
    public void setListener(OfferingsAsyncTaskListener l) {
        mListener = l;
    }
    
    @Override
    protected void onPostExecute(ArrayList<OfferingBean> result) {
        super.onPostExecute(result);
        
        if (result != null) {
            if (mListener != null) {
                mListener.onSuccess(result);
            }
        } else {
            if (mListener != null) {
                mListener.onError(mException);
            }
        }
    }
    
    public interface OfferingsAsyncTaskListener {
        public void onSuccess(ArrayList<OfferingBean> offerings);
        public void onError(Throwable e);
    }

}
