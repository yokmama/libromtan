package jp.co.kayo.android.librom;

import java.util.ArrayList;

import jp.co.kayo.android.librom.bean.OfferingBean;
import jp.co.kayo.android.librom.net.OfferingsAsyncTask;
import jp.co.kayo.android.librom.net.OfferingsAsyncTask.OfferingsAsyncTaskListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.widget.Toast;

public class TestOfferingsListFragment extends ListFragment {

    private static final String DLG_PROGRESS = "dlg-progress";
    private ProgressDialogFragment mProgressDialog;
    protected ArrayList<OfferingBean> mOfferings;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mProgressDialog = new ProgressDialogFragment();
        mProgressDialog.show(getChildFragmentManager(), DLG_PROGRESS);
        
        final Context context = getActivity().getApplicationContext();

        if (mOfferings == null) {
            OfferingsAsyncTask task = new OfferingsAsyncTask();
            task.setListener(new OfferingsAsyncTaskListener() {
                @Override
                public void onSuccess(ArrayList<OfferingBean> offerings) {
                    onFinish();
                    mOfferings = offerings;
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        setList();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    onFinish();
                    Toast.makeText(context, "通信失敗", Toast.LENGTH_SHORT).show();
                }

                private void onFinish() {
                    mProgressDialog.dismissAllowingStateLoss();
                }
            });
            task.execute();
        } else {
            setList();
        }
    }

    protected void setList() {
        OfferingListAdapter adapter = new OfferingListAdapter(getActivity().getApplicationContext(), mOfferings);
        setListAdapter(adapter);
    }

    /**
     * 進捗ダイアログ
     * 
     * @author esperia09
     */
    public static class ProgressDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            ProgressDialog dlg = new ProgressDialog(getActivity());
            dlg.setMessage("データを取得しています…。"); // TODO
            return dlg;
        }
    }
}
