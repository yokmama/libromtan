
package jp.co.kayo.android.librom.bean;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 供物データ
 * 
 * @author esperia09
 */
public class OfferingBean {

    private String name;
    private String power;
    private String[] star;
    private String effectTime;
    private String howToGet;
    private String effect;
    /** @deprecated */
    private String img;

    public static ArrayList<OfferingBean> parse(String raw) throws JSONException {
        ArrayList<OfferingBean> list = new ArrayList<OfferingBean>();

        JSONObject root = new JSONObject(raw);
        JSONArray jsonArray = root.getJSONArray("offerings");
        for (int i = 0, iL = jsonArray.length(); i < iL; i++) {
            JSONObject ofrJson = jsonArray.getJSONObject(i);

            OfferingBean bean = new OfferingBean();
            bean.setName(ofrJson.getString("name"));
            bean.setPower(ofrJson.getString("power"));
            JSONArray starArray = ofrJson.getJSONArray("star");
            bean.setStar(new String[] {
                    starArray.getString(0),
                    starArray.getString(1),
                    starArray.getString(2),
                    starArray.getString(3),
            });
            bean.setEffectTime(ofrJson.getString("effect_time"));
            bean.setHowToGet(ofrJson.getString("how_to_get"));
            bean.setEffect(ofrJson.getString("effect"));
            bean.img = ofrJson.getString("img");

            list.add(bean);
        }

        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String[] getStar() {
        return star;
    }

    public void setStar(String[] star) {
        this.star = star;
    }

    public String getEffectTime() {
        return effectTime;
    }

    public void setEffectTime(String effectTime) {
        this.effectTime = effectTime;
    }

    public String getHowToGet() {
        return howToGet;
    }

    public void setHowToGet(String howToGet) {
        this.howToGet = howToGet;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }
}
