package jp.co.kayo.android.librom.db;

import jp.co.kayo.android.librom.db.TableColumns.*;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

class LibromDatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "libromdatabase.db";
    private static final int DATABASE_VERSION = 1;

    public LibromDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // @formatter:off
        db.beginTransaction();
        try {
            if (!findTable(db, LastUpdate.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(LastUpdate.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(LastUpdate.DATE).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Category.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Category.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Category.NAME).append(" TEXT");
                sql.append(",").append(Category.DESCRIPTION).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Attribute.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Attribute.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Attribute.NAME).append(" TEXT");
                sql.append(",").append(Attribute.DESCRIPTION).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Effect.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Effect.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Effect.NAME).append(" TEXT");
                sql.append(",").append(Effect.DESCRIPTION).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Sacrifice.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Sacrifice.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Sacrifice.NAME).append(" TEXT");
                sql.append(",").append(Sacrifice.CATEGORY_ID).append(" LONG");
                sql.append(",").append(Sacrifice.ATTRIBUTE_ID).append(" LONG");
                sql.append(",").append(Sacrifice.EFFECT_ID).append(" LONG");
                sql.append(",").append(Sacrifice.POWER).append(" INTEGER");
                sql.append(",").append(Sacrifice.NUMBER).append(" INTEGER");
                sql.append(",").append(Sacrifice.EFFECT_TIME).append(" INTEGER");
                sql.append(",").append(Sacrifice.IMAGE).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, SacrificeDescription.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(SacrificeDescription.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(SacrificeDescription.NAME).append(" TEXT");
                sql.append(",").append(SacrificeDescription.SACRIFICE_ID).append(" LONG");
                sql.append(",").append(SacrificeDescription.DESCRIPTION).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Quest.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Quest.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Quest.NAME).append(" TEXT");
                sql.append(",").append(Quest.INDEX_ID).append(" LONG");
                sql.append(",").append(Quest.DESCRIPTION).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, QuestSacrifice.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(QuestSacrifice.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(QuestSacrifice.QUEST_ID).append(" LONG");
                sql.append(",").append(QuestSacrifice.SACRIFICE_ID).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, Enemy.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(Enemy.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(Enemy.NAME).append(" TEXT");
                sql.append(",").append(Enemy.TYPE).append(" INTEGER");
                sql.append(",").append(Enemy.DESCRIPTION).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, QuestEnemy.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(QuestEnemy.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(QuestEnemy.QUEST_ID).append(" LONG");
                sql.append(",").append(QuestEnemy.ENEMY_ID).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, EnemyAttribute.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(EnemyAttribute.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(EnemyAttribute.ENEMY_ID).append(" LONG");
                sql.append(",").append(EnemyAttribute.ATTRIBUTE_ID).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, EnemyWeakAttribute.TBNAME)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(EnemyWeakAttribute.TBNAME).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(EnemyWeakAttribute.ENEMY_ID).append(" LONG");
                sql.append(",").append(EnemyWeakAttribute.ATTRIBUTE_ID).append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // TODO: Indexテーブルが抜けてます！

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        // @formatter:on
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVewsion) {
        onCreate(db);
    }

    public boolean findTable(SQLiteDatabase db, String tbl) {

        StringBuilder where = new StringBuilder();
        where.append("type='table' and name='");
        where.append(tbl);
        where.append("'");

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("sqlite_master"); // テーブル名
        Cursor cur = null;
        try {
            cur = qb.query(db, null, where.toString(), null, null, null, null,
                    null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    return true;
                }
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public int delete(String table, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.delete(table, selection, selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    public long insert(String table, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(table, null, values);
            db.setTransactionSuccessful();
            return id;
        } finally {
            db.endTransaction();
        }
    }

    public Cursor query(String table, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getReadableDatabase();
        try {
            return db.query(table, projection, selection, selectionArgs, null,
                    null, sortOrder);
        } finally {
        }
    }

    public int update(String table, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.update(table, values, selection, selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }
}
