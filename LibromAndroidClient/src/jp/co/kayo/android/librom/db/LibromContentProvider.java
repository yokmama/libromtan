package jp.co.kayo.android.librom.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public class LibromContentProvider extends ContentProvider implements
        TableColumns {
    public static final String AUTHORITY = "jp.co.kayo.android.librom";
    public static final String CONTENT_AUTHORITY = "content://" + AUTHORITY + "/";
    public static final Uri LASTUPDATE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
            + LastUpdate.TBNAME);
    public static final Uri CATEGORY_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + Category.TBNAME);
    public static final Uri ATTRIBUTE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + Attribute.TBNAME);
    public static final Uri EFFECT_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + Effect.TBNAME);
    public static final Uri SACRIFICE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + Sacrifice.TBNAME);
    public static final Uri SACRIFICEDESCRIPTION_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
            + SacrificeDescription.TBNAME);
    public static final Uri INDEX_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + Index.TBNAME);
    public static final Uri QUEST_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + Quest.TBNAME);
    public static final Uri QUESTSACRIFICE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
            + QuestSacrifice.TBNAME);
    public static final Uri ENEMY_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY + Enemy.TBNAME);
    public static final Uri QUESTENEMY_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
            + QuestEnemy.TBNAME);
    public static final Uri ENEMYATTRIBUTE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
            + EnemyAttribute.TBNAME);
    public static final Uri ENEMYWEAKATTRIBUTE_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY
            + EnemyWeakAttribute.TBNAME);

    public static final int CODE_LASTUPDATE = 0;
    public static final int CODE_LASTUPDATE_ID = 1;
    public static final int CODE_CATEGORY = 2;
    public static final int CODE_CATEGORY_ID = 3;
    public static final int CODE_ATTRIBUTE = 4;
    public static final int CODE_ATTRIBUTE_ID = 5;
    public static final int CODE_EFFECT = 6;
    public static final int CODE_EFFECT_ID = 7;
    public static final int CODE_SACRIFICE = 8;
    public static final int CODE_SACRIFICE_ID = 9;
    public static final int CODE_SACRIFICEDESCRIPTION = 10;
    public static final int CODE_SACRIFICEDESCRIPTION_ID = 11;
    public static final int CODE_INDEX = 12;
    public static final int CODE_INDEX_ID = 13;
    public static final int CODE_QUEST = 14;
    public static final int CODE_QUEST_ID = 15;
    public static final int CODE_QUESTSACRIFICE = 16;
    public static final int CODE_QUESTSACRIFICE_ID = 17;
    public static final int CODE_ENEMY = 18;
    public static final int CODE_ENEMY_ID = 19;
    public static final int CODE_QUESTENEMY = 20;
    public static final int CODE_QUESTENEMY_ID = 21;
    public static final int CODE_ENEMYATTRIBUTE = 22;
    public static final int CODE_ENEMYATTRIBUTE_ID = 23;
    public static final int CODE_ENEMYWEAKATTRIBUTE = 24;
    public static final int CODE_ENEMYWEAKATTRIBUTE_ID = 25;

    private LibromDatabaseHelper mDatabaseHelper = null;
    private UriMatcher mUriMatcher = null;

    @Override
    public boolean onCreate() {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(AUTHORITY, LastUpdate.TBNAME, CODE_LASTUPDATE);
        mUriMatcher.addURI(AUTHORITY, LastUpdate.TBNAME + "/#", CODE_LASTUPDATE_ID);
        mUriMatcher.addURI(AUTHORITY, Category.TBNAME, CODE_CATEGORY);
        mUriMatcher.addURI(AUTHORITY, Category.TBNAME + "/#", CODE_CATEGORY_ID);
        mUriMatcher.addURI(AUTHORITY, Attribute.TBNAME, CODE_ATTRIBUTE);
        mUriMatcher.addURI(AUTHORITY, Attribute.TBNAME + "/#", CODE_ATTRIBUTE_ID);
        mUriMatcher.addURI(AUTHORITY, Effect.TBNAME, CODE_EFFECT);
        mUriMatcher.addURI(AUTHORITY, Effect.TBNAME + "/#", CODE_EFFECT_ID);
        mUriMatcher.addURI(AUTHORITY, Sacrifice.TBNAME, CODE_SACRIFICE);
        mUriMatcher.addURI(AUTHORITY, Sacrifice.TBNAME + "/#", CODE_SACRIFICE_ID);
        mUriMatcher.addURI(AUTHORITY, SacrificeDescription.TBNAME, CODE_SACRIFICEDESCRIPTION);
        mUriMatcher.addURI(AUTHORITY, SacrificeDescription.TBNAME + "/#",
                CODE_SACRIFICEDESCRIPTION_ID);
        mUriMatcher.addURI(AUTHORITY, Index.TBNAME, CODE_INDEX);
        mUriMatcher.addURI(AUTHORITY, Index.TBNAME + "/#", CODE_INDEX_ID);
        mUriMatcher.addURI(AUTHORITY, Quest.TBNAME, CODE_QUEST);
        mUriMatcher.addURI(AUTHORITY, Quest.TBNAME + "/#", CODE_QUEST_ID);
        mUriMatcher.addURI(AUTHORITY, QuestSacrifice.TBNAME, CODE_QUESTSACRIFICE);
        mUriMatcher.addURI(AUTHORITY, QuestSacrifice.TBNAME + "/#", CODE_QUESTSACRIFICE_ID);
        mUriMatcher.addURI(AUTHORITY, Enemy.TBNAME, CODE_ENEMY);
        mUriMatcher.addURI(AUTHORITY, Enemy.TBNAME + "/#", CODE_ENEMY_ID);
        mUriMatcher.addURI(AUTHORITY, QuestEnemy.TBNAME, CODE_QUESTENEMY);
        mUriMatcher.addURI(AUTHORITY, QuestEnemy.TBNAME + "/#", CODE_QUESTENEMY_ID);
        mUriMatcher.addURI(AUTHORITY, EnemyAttribute.TBNAME, CODE_ENEMYATTRIBUTE);
        mUriMatcher.addURI(AUTHORITY, EnemyAttribute.TBNAME + "/#", CODE_ENEMYATTRIBUTE_ID);
        mUriMatcher.addURI(AUTHORITY, EnemyWeakAttribute.TBNAME, CODE_ENEMYWEAKATTRIBUTE);
        mUriMatcher.addURI(AUTHORITY, EnemyWeakAttribute.TBNAME + "/#", CODE_ENEMYWEAKATTRIBUTE_ID);

        mDatabaseHelper = new LibromDatabaseHelper(getContext());

        return true;
    }

    @Override
    public String getType(Uri arg0) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_LASTUPDATE:
                return delete(LastUpdate.TBNAME, selection, selectionArgs);
            case CODE_LASTUPDATE_ID:
                return delete(LastUpdate.TBNAME, uri, selection, selectionArgs);
            case CODE_CATEGORY:
                return delete(Category.TBNAME, selection, selectionArgs);
            case CODE_CATEGORY_ID:
                return delete(Category.TBNAME, uri, selection, selectionArgs);
            case CODE_ATTRIBUTE:
                return delete(Attribute.TBNAME, selection, selectionArgs);
            case CODE_ATTRIBUTE_ID:
                return delete(Attribute.TBNAME, uri, selection, selectionArgs);
            case CODE_EFFECT:
                return delete(Effect.TBNAME, selection, selectionArgs);
            case CODE_EFFECT_ID:
                return delete(Effect.TBNAME, uri, selection, selectionArgs);
            case CODE_SACRIFICE:
                return delete(Sacrifice.TBNAME, selection, selectionArgs);
            case CODE_SACRIFICE_ID:
                return delete(Sacrifice.TBNAME, uri, selection, selectionArgs);
            case CODE_SACRIFICEDESCRIPTION:
                return delete(SacrificeDescription.TBNAME, selection, selectionArgs);
            case CODE_SACRIFICEDESCRIPTION_ID:
                return delete(SacrificeDescription.TBNAME, uri, selection, selectionArgs);
            case CODE_INDEX:
                return delete(Index.TBNAME, selection, selectionArgs);
            case CODE_INDEX_ID:
                return delete(Index.TBNAME, uri, selection, selectionArgs);
            case CODE_QUEST:
                return delete(Quest.TBNAME, selection, selectionArgs);
            case CODE_QUEST_ID:
                return delete(Quest.TBNAME, uri, selection, selectionArgs);
            case CODE_QUESTSACRIFICE:
                return delete(QuestSacrifice.TBNAME, selection, selectionArgs);
            case CODE_QUESTSACRIFICE_ID:
                return delete(QuestSacrifice.TBNAME, uri, selection, selectionArgs);
            case CODE_ENEMY:
                return delete(Enemy.TBNAME, selection, selectionArgs);
            case CODE_ENEMY_ID:
                return delete(Enemy.TBNAME, uri, selection, selectionArgs);
            case CODE_QUESTENEMY:
                return delete(QuestEnemy.TBNAME, selection, selectionArgs);
            case CODE_QUESTENEMY_ID:
                return delete(QuestEnemy.TBNAME, uri, selection, selectionArgs);
            case CODE_ENEMYATTRIBUTE:
                return delete(EnemyAttribute.TBNAME, selection, selectionArgs);
            case CODE_ENEMYATTRIBUTE_ID:
                return delete(EnemyAttribute.TBNAME, uri, selection, selectionArgs);
            case CODE_ENEMYWEAKATTRIBUTE:
                return delete(EnemyWeakAttribute.TBNAME, selection, selectionArgs);
            case CODE_ENEMYWEAKATTRIBUTE_ID:
                return delete(EnemyWeakAttribute.TBNAME, uri, selection, selectionArgs);
        }
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_LASTUPDATE:
                return ContentUris.withAppendedId(LASTUPDATE_CONTENT_URI,
                        mDatabaseHelper.insert(LastUpdate.TBNAME, values));
            case CODE_CATEGORY:
                return ContentUris.withAppendedId(CATEGORY_CONTENT_URI,
                        mDatabaseHelper.insert(Category.TBNAME, values));
            case CODE_ATTRIBUTE:
                return ContentUris.withAppendedId(ATTRIBUTE_CONTENT_URI,
                        mDatabaseHelper.insert(Attribute.TBNAME, values));
            case CODE_EFFECT:
                return ContentUris.withAppendedId(EFFECT_CONTENT_URI,
                        mDatabaseHelper.insert(Effect.TBNAME, values));
            case CODE_SACRIFICE:
                return ContentUris.withAppendedId(SACRIFICE_CONTENT_URI,
                        mDatabaseHelper.insert(Sacrifice.TBNAME, values));
            case CODE_SACRIFICEDESCRIPTION:
                return ContentUris.withAppendedId(SACRIFICEDESCRIPTION_CONTENT_URI,
                        mDatabaseHelper.insert(SacrificeDescription.TBNAME, values));
            case CODE_INDEX:
                return ContentUris.withAppendedId(INDEX_CONTENT_URI,
                        mDatabaseHelper.insert(Index.TBNAME, values));
            case CODE_QUEST:
                return ContentUris.withAppendedId(QUEST_CONTENT_URI,
                        mDatabaseHelper.insert(Quest.TBNAME, values));
            case CODE_QUESTSACRIFICE:
                return ContentUris.withAppendedId(QUESTSACRIFICE_CONTENT_URI,
                        mDatabaseHelper.insert(QuestSacrifice.TBNAME, values));
            case CODE_ENEMY:
                return ContentUris.withAppendedId(ENEMY_CONTENT_URI,
                        mDatabaseHelper.insert(Enemy.TBNAME, values));
            case CODE_QUESTENEMY:
                return ContentUris.withAppendedId(QUESTENEMY_CONTENT_URI,
                        mDatabaseHelper.insert(QuestEnemy.TBNAME, values));
            case CODE_ENEMYATTRIBUTE:
                return ContentUris.withAppendedId(ENEMYATTRIBUTE_CONTENT_URI,
                        mDatabaseHelper.insert(EnemyAttribute.TBNAME, values));
            case CODE_ENEMYWEAKATTRIBUTE:
                return ContentUris.withAppendedId(ENEMYWEAKATTRIBUTE_CONTENT_URI,
                        mDatabaseHelper.insert(EnemyWeakAttribute.TBNAME, values));
        }
        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_LASTUPDATE:
                return query(LastUpdate.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_LASTUPDATE_ID:
                return query(LastUpdate.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_CATEGORY:
                return query(Category.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_CATEGORY_ID:
                return query(Category.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ATTRIBUTE:
                return query(Attribute.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ATTRIBUTE_ID:
                return query(Attribute.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_EFFECT:
                return query(Effect.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_EFFECT_ID:
                return query(Effect.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_SACRIFICE:
                return query(Sacrifice.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_SACRIFICE_ID:
                return query(Sacrifice.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_SACRIFICEDESCRIPTION:
                return query(SacrificeDescription.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_SACRIFICEDESCRIPTION_ID:
                return query(SacrificeDescription.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_INDEX:
                return query(Index.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_INDEX_ID:
                return query(Index.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_QUEST:
                return query(Quest.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_QUEST_ID:
                return query(Quest.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_QUESTSACRIFICE:
                return query(QuestSacrifice.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_QUESTSACRIFICE_ID:
                return query(QuestSacrifice.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ENEMY:
                return query(Enemy.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ENEMY_ID:
                return query(Enemy.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_QUESTENEMY:
                return query(QuestEnemy.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_QUESTENEMY_ID:
                return query(QuestEnemy.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ENEMYATTRIBUTE:
                return query(EnemyAttribute.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ENEMYATTRIBUTE_ID:
                return query(EnemyAttribute.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ENEMYWEAKATTRIBUTE:
                return query(EnemyWeakAttribute.TBNAME, projection,
                        selection, selectionArgs, sortOrder);
            case CODE_ENEMYWEAKATTRIBUTE_ID:
                return query(EnemyWeakAttribute.TBNAME, uri, projection,
                        selection, selectionArgs, sortOrder);
        }
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_LASTUPDATE:
                return update(LastUpdate.TBNAME, values, selection, selectionArgs);
            case CODE_LASTUPDATE_ID:
                return update(LastUpdate.TBNAME, uri, values, selection, selectionArgs);
            case CODE_CATEGORY:
                return update(Category.TBNAME, values, selection, selectionArgs);
            case CODE_CATEGORY_ID:
                return update(Category.TBNAME, uri, values, selection, selectionArgs);
            case CODE_ATTRIBUTE:
                return update(Attribute.TBNAME, values, selection, selectionArgs);
            case CODE_ATTRIBUTE_ID:
                return update(Attribute.TBNAME, uri, values, selection, selectionArgs);
            case CODE_EFFECT:
                return update(Effect.TBNAME, values, selection, selectionArgs);
            case CODE_EFFECT_ID:
                return update(Effect.TBNAME, uri, values, selection, selectionArgs);
            case CODE_SACRIFICE:
                return update(Sacrifice.TBNAME, values, selection, selectionArgs);
            case CODE_SACRIFICE_ID:
                return update(Sacrifice.TBNAME, uri, values, selection, selectionArgs);
            case CODE_SACRIFICEDESCRIPTION:
                return update(SacrificeDescription.TBNAME, values, selection, selectionArgs);
            case CODE_SACRIFICEDESCRIPTION_ID:
                return update(SacrificeDescription.TBNAME, uri, values, selection, selectionArgs);
            case CODE_INDEX:
                return update(Index.TBNAME, values, selection, selectionArgs);
            case CODE_INDEX_ID:
                return update(Index.TBNAME, uri, values, selection, selectionArgs);
            case CODE_QUEST:
                return update(Quest.TBNAME, values, selection, selectionArgs);
            case CODE_QUEST_ID:
                return update(Quest.TBNAME, uri, values, selection, selectionArgs);
            case CODE_QUESTSACRIFICE:
                return update(QuestSacrifice.TBNAME, values, selection, selectionArgs);
            case CODE_QUESTSACRIFICE_ID:
                return update(QuestSacrifice.TBNAME, uri, values, selection, selectionArgs);
            case CODE_ENEMY:
                return update(Enemy.TBNAME, values, selection, selectionArgs);
            case CODE_ENEMY_ID:
                return update(Enemy.TBNAME, uri, values, selection, selectionArgs);
            case CODE_QUESTENEMY:
                return update(QuestEnemy.TBNAME, values, selection, selectionArgs);
            case CODE_QUESTENEMY_ID:
                return update(QuestEnemy.TBNAME, uri, values, selection, selectionArgs);
            case CODE_ENEMYATTRIBUTE:
                return update(EnemyAttribute.TBNAME, values, selection, selectionArgs);
            case CODE_ENEMYATTRIBUTE_ID:
                return update(EnemyAttribute.TBNAME, uri, values, selection, selectionArgs);
            case CODE_ENEMYWEAKATTRIBUTE:
                return update(EnemyWeakAttribute.TBNAME, values, selection, selectionArgs);
            case CODE_ENEMYWEAKATTRIBUTE_ID:
                return update(EnemyWeakAttribute.TBNAME, uri, values, selection, selectionArgs);
        }
        return 0;
    }

    private int delete(String tbname, String selection, String[] selectionArgs) {
        return mDatabaseHelper.delete(tbname, selection, selectionArgs);
    }

    private int delete(String tbname, Uri uri, String selection, String[] selectionArgs) {
        long id = ContentUris.parseId(uri);
        return mDatabaseHelper.delete(tbname, BaseColumns._ID
                + " = ?", new String[] {
                Long.toString(id)
        });
    }

    private Cursor query(String tbname, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        return mDatabaseHelper.query(tbname, projection, selection, selectionArgs, sortOrder);
    }

    private Cursor query(String tbname, Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        long id = ContentUris.parseId(uri);
        return mDatabaseHelper.query(tbname, projection,
                BaseColumns._ID + " = ?", new String[] {
                    Long.toString(id)
                },
                sortOrder);
    }

    private int update(String tbname, ContentValues values, String selection,
            String[] selectionArgs) {
        return mDatabaseHelper.update(tbname, values, selection,
                selectionArgs);
    }

    private int update(String tbname, Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        long id = ContentUris.parseId(uri);
        return mDatabaseHelper.update(tbname, values, BaseColumns._ID
                + " = ?", new String[] {
                Long.toString(id)
        });
    }

}
