
package jp.co.kayo.android.librom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.add("show/hide list");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        CharSequence title = item.getTitle();
        if (TextUtils.equals(title, "show/hide list")) {
            FragmentManager fm = getSupportFragmentManager();
            Fragment f = fm.findFragmentById(android.R.id.content);
            FragmentTransaction ft = fm.beginTransaction();
            if (f != null) {
                ft.remove(f);
            } else {
                ft.add(android.R.id.content, new TestOfferingsListFragment());
            }
            ft.commit();
        }
        return super.onOptionsItemSelected(item);
    }

}
