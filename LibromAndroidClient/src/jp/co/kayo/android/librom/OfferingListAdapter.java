package jp.co.kayo.android.librom;

import java.util.ArrayList;

import jp.co.kayo.android.librom.bean.OfferingBean;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class OfferingListAdapter extends BaseAdapter {
    private ArrayList<OfferingBean> mList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public OfferingListAdapter(Context context, ArrayList<OfferingBean> list) {
        mList = list;
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            v = (ViewGroup) mLayoutInflater.inflate(R.layout.row, null);
        } else {
            v = convertView;
        }
        OfferingBean offeringBean = mList.get(position);
        
        TextView textView = (TextView) v.findViewById(R.id.text);
        
        textView.setText(offeringBean.getName());
        
        return v;
    }
}
